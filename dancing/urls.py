"""dancing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^$', 'Core.views.index', name='index'),
    url(r'Bailarin/edita/$', ('Bailarin.views.edita')),
    url(r'Bailarin/(?P<id>\d*)',('Bailarin.views.DetalleBailarin')),
    url(r'mision/$',('Core.views.mision')),
    url(r'contacto/$',('Core.views.contacto')),
    url(r'desarrollador/$',('Core.views.desarrollador')),
    url(r'Usuario/login/$', ('Usuario.views.loginUsr')),
    url(r'Usuario/registro/$', ('Usuario.views.registroUsr')),
    url(r'Usuario/logout/$', ('Usuario.views.logoutUsr')),
    url(r'Usuario/panel/(?P<id>\d*)', ('Usuario.views.panel')),
    url(r'Eventos/nuevo/$', ('Eventos.views.crearEvento')),
    url(r'Eventos/cancelar/(?P<id_evento>\d*)', ('Eventos.views.cancelar')),
    url(r'^rosetta/', include('rosetta.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
	)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
        }),
)