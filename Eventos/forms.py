from django.forms import ModelForm
from Eventos.models import Eventos

class EventoForm(ModelForm):
	class Meta:
		model = Eventos
		fields = ['nombre','fecha_realizacion','duracion','bailarines','position']