from django.db import models
from django.contrib.auth.models import User
from geoposition.fields import GeopositionField
from .models import *


class Eventos(models.Model):
	nombre = models.CharField(max_length=255, null=True,blank=True)
	creador = models.ForeignKey(User)
	fecha_creacion = models.DateField(blank=True, null=True, auto_now_add=True)
	fecha_realizacion = models.DateField(blank=True, null=True)
	duracion = models.DecimalField(decimal_places=1, max_digits=2)
	bailarines = models.ManyToManyField('Bailarin.Bailarin')
	position = GeopositionField()
	activo = models.BooleanField(default=True)


	def __str__(self):
		return self.nombre + " " + str(self.fecha_realizacion)
