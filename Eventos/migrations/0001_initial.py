# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import geoposition.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Bailarin', '0007_auto_20151013_2335'),
    ]

    operations = [
        migrations.CreateModel(
            name='Eventos',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('nombre', models.CharField(null=True, max_length=255, blank=True)),
                ('fecha_creacion', models.DateField(null=True, auto_now_add=True)),
                ('fecha_realizacion', models.DateField(null=True, blank=True)),
                ('duracion', models.DecimalField(decimal_places=1, max_digits=2)),
                ('position', geoposition.fields.GeopositionField(max_length=42)),
                ('activo', models.BooleanField(default=True)),
                ('bailarines', models.ManyToManyField(to='Bailarin.Bailarin')),
                ('creador', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
