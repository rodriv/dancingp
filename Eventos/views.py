from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from Bailarin.models import Bailarin
from .forms import *
from django.template import RequestContext

# Create your views here.

@login_required(login_url='/Usuario/login/')
def crearEvento(request):
	user = request.user
	if request.method == 'POST':
		form = EventoForm(request.POST)
		if form.is_valid():
			evento = form.save(commit=False)
			evento.creador = request.user			
			evento.save()
			form.save_m2m()
			return render_to_response("eventoCreado.html", {"Usr": user}, context_instance=RequestContext(request))
	else:
		form = EventoForm()

	return render(request, 'nuevoEvento.html',{'form':form})


@login_required(login_url='/Usuario/login/')
def cancelar(request, id_evento):
	user = request.user
	evt = Eventos.objects.get(pk=id_evento)
	evt.activo = False
	evt.save()	

	return redirect("Usuario/panel/" + str(user.id))







