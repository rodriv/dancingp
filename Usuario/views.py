from django.shortcuts import render, render_to_response, redirect
from .forms import *
from .models import *
from django.template import RequestContext
from django.db.models import *
from django.contrib.auth import authenticate, login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from Bailarin.models import Bailarin
from Eventos.models import Eventos

def loginUsr(request):	
	user=""
	if request.method=='POST':
		form = formLogin(request.POST)
		if form.is_valid():
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username=username, password=password)
			if user != None:
				if user.is_active:
					login(request, user)
					return redirect("Usuario/panel/" + str(user.id))
				else:
					return render(request, 'login.html', {'form':form, 'error':'EL usuario aún no ha sido activado por el administrador.'})
			else:
				return render(request, 'login.html', {'form':form, 'error':'No existe ningún usuario con los datos ingresados'})
		else:
			return render(request, 'login.html', {'form':form, 'error':'Error en los datos del formulario'})
	else:
		form=formLogin()
		return render(request,'login.html', {"form":form})

def registroUsr(request):
	if request.method == 'POST':
		form = formRegistro(request.POST)
		if form.is_valid():
			if not User.objects.filter(username=request.POST["email"]):
				if request.POST['password'] == request.POST['confpassword']:
					username = request.POST['email']
					password = request.POST['password']
					email = request.POST['email']
					nombre = request.POST['nombre']
					apellido = request.POST['apellido']
					user = User.objects.create_user(username=username,email= email, password =password,first_name=nombre,last_name=apellido)
					user.is_active = False
					user.save()
					return render(request,'registroCorrecto.html',{})
				else:
					return render(request,'registro.html',{'form':form ,'error':'Las passwords ingresadas no coinciden'})
			else:
				return render(request,'registro.html',{'form':form,'error':'El email que ingreso ya esta registrado'})
		else:
			form = formRegistro()
			return render(request,'registro.html',{'form':form,'error':'Error al ingresar el formulario'})
	else:
		form = formRegistro()
		return render(request,'registro.html',{'form':form,'error':''})



@login_required(login_url='/Usuario/login/')
def panel(request, id):
	if request.user.id == int(id):
		try:
			b = Bailarin.objects.get(usuario = User.objects.get(pk=int(id)))
		except Bailarin.DoesNotExist:
			b = None

		print(None==b)
		if b!=None:				
			print ("#### b existe, asi que es Bailarin")
			eventos = Eventos.objects.filter(bailarines__id=b.id).filter(activo=True).order_by("-fecha_creacion")			
			return render_to_response('panel.html', {"Bailarin": b, "Eventos":eventos}, context_instance=RequestContext(request))
		else:
			print ("#### b es None, asi que es Cliente")
			#busco para devolver solo el usr con ese id
			c = request.user
			eventos = Eventos.objects.filter(creador=c).filter(activo=True).order_by("-fecha_creacion")
			return render_to_response('panelCliente.html', {"Usr": c, "Eventos": eventos}, context_instance=RequestContext(request))
	else:
		return HttpResponse("Acceso denegado")

def logoutUsr(request):
	auth_logout(request)
	return redirect('/')
