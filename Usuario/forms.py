from django import forms
from .models import *

class formRegistro(forms.Form):
    email= forms.EmailField(label='Email',widget=forms.TextInput(attrs={'class' : 'validate','required':''}))
    password= forms.password=forms.CharField(max_length=30, label='Password',widget=forms.PasswordInput(attrs={'class': 'validate','required':''}))
    confpassword= forms.password=forms.CharField(max_length=30, label='Repetir Password',widget=forms.PasswordInput(attrs={'class': 'validate','required':''}))
    nombre= forms.CharField(max_length=30,label='Nombre',widget=forms.TextInput(attrs={'class': 'validate','required':''}))
    apellido= forms.CharField(max_length=30, label='Apellido',widget=forms.TextInput(attrs={'class': 'validate','required':''}))


class formLogin(forms.Form):
    username = forms.CharField(max_length=30, label='Usuario',widget=forms.TextInput(attrs={'class' : 'validate','required':''}))
    password = forms.CharField(max_length=30, label='Contrasena',widget=forms.PasswordInput(attrs={'class': 'validate','required':''}))