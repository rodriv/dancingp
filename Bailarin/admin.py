from django.contrib import admin
from Bailarin.models import *
from Eventos.models import *
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User


# Register your models here.

admin.site.register(Bailarin)
admin.site.register(TipoBailarin)
admin.site.register(Multimedia)
admin.site.register(TipoMultimedia)
admin.site.register(Ciudad)
admin.site.register(Provincia)
admin.site.register(Curriculum)
admin.site.register(Eventos)
admin.site.unregister(User)

class MyUserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'first_name', 'last_name', 'password1', 'password2')}
        ),
    )

admin.site.register(User, MyUserAdmin)
