from django.db import models
from django.contrib.auth.models import User


class Bailarin(models.Model):
    usuario =models.OneToOneField(User)    
    fecha_nacimiento = models.DateField(blank=True, null=True)
    fecha_alta = models.DateField(blank=True, null=True)
    ciudad = models.ForeignKey('Ciudad',null =True,blank=True)
    direccion = models.CharField(max_length=50, null=True,blank=True)
    mail = models.EmailField(null=False)    
    telefono = models.CharField(blank=True, max_length=15) # validators should be a list
    tipo_bailarin = models.ManyToManyField('TipoBailarin')
    foto_perfil = models.ImageField(upload_to ='perfiles',blank=True,null=True)

    
    def __str__(self):
    	return self.usuario.get_full_name() 


class TipoBailarin(models.Model):
	descripcion = models.CharField(max_length=255, null=False, blank=False)

	def __str__(self):
		return self.descripcion


class Multimedia(models.Model):
	nombre = models.CharField(max_length=255, null=True,blank=True)
	fecha_alta = models.DateField(blank=True, null=True)
	bailarin = models.ForeignKey('Bailarin')
	tipo_multimedia = models.ForeignKey('TipoMultimedia')


class TipoMultimedia(models.Model):
	descripcion = models.CharField(max_length=255, null=False, blank=False)

	def __str__(self):
		return self.descripcion


class Provincia(models.Model):
	nombre = models.CharField(max_length=50)
	
	def __str__(self):
		return self.nombre


class Ciudad(models.Model):
	nombre= models.CharField(max_length=100)
	provincia_id= models.ForeignKey('Provincia')
	
	def __str__(self):
		return self.nombre


class Curriculum(models.Model):
	bailarin = models.OneToOneField(Bailarin)
	formacion_academica = models.TextField()
	experiencia_profesional = models.TextField()
	idiomas = models.TextField()
	video1 = models.URLField(blank=True, null=True)
	video2 = models.URLField(blank=True, null=True)
	video3 = models.URLField(blank=True, null=True)

	def __str__(self):
		return self.bailarin.usuario.username + " CV"



