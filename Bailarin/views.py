from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from Bailarin.models import *
from Bailarin.forms import CVModelForm
from django.contrib.auth.decorators import login_required

# Create your views here.

def buscarBailarin(id):
	b=get_object_or_404(Bailarin, pk=id)
	return b

def DetalleBailarin(request,id):
	bailarin = buscarBailarin(id)
	videos = []
	if bailarin.curriculum.video1!="":
		videos.append(bailarin.curriculum.video1)

	if bailarin.curriculum.video2!="":
		videos.append(bailarin.curriculum.video2)

	if bailarin.curriculum.video3!="":
		videos.append(bailarin.curriculum.video3)

	print(len(videos))
	return render_to_response('bailarin.html',{"Bailarin": bailarin, "Videos":videos},context_instance=RequestContext(request))


@login_required(login_url='/Usuario/login/')
def edita(request):	
	cv = Curriculum.objects.get(bailarin = Bailarin.objects.get(usuario = request.user))
	if request.method == 'POST':		
		form = CVModelForm(request.POST, instance=cv)
		if form.is_valid():			
			form.save()
			print("CV actualizado")
			return redirect("/")
	else:		
		form = CVModelForm(instance=cv)

	return render(request, 'editacv.html',{'form':form})
