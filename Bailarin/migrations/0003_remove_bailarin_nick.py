# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Bailarin', '0002_auto_20151013_2103'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bailarin',
            name='nick',
        ),
    ]
