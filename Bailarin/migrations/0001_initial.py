# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bailarin',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('nick', models.CharField(blank=True, max_length=50, null=True)),
                ('fecha_nacimiento', models.DateField(blank=True, null=True)),
                ('fecha_alta', models.DateField(blank=True, null=True)),
                ('direccion', models.CharField(blank=True, max_length=50, null=True)),
                ('mail', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Multimedia',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('nombre', models.CharField(blank=True, max_length=255, null=True)),
                ('fecha_alta', models.DateField(blank=True, null=True)),
                ('bailarin', models.ForeignKey(to='Bailarin.Bailarin')),
            ],
        ),
        migrations.CreateModel(
            name='Provincias',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='TipoBailarin',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('descripcion', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='TipoMultimedia',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('descripcion', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='multimedia',
            name='tipo_multimedia',
            field=models.ForeignKey(to='Bailarin.TipoMultimedia'),
        ),
        migrations.AddField(
            model_name='ciudad',
            name='provincia_id',
            field=models.ForeignKey(to='Bailarin.Provincias'),
        ),
        migrations.AddField(
            model_name='bailarin',
            name='ciudad',
            field=models.ForeignKey(to='Bailarin.Ciudad', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bailarin',
            name='tipo_bailarin',
            field=models.ManyToManyField(to='Bailarin.TipoBailarin'),
        ),
        migrations.AddField(
            model_name='bailarin',
            name='usuario',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
    ]
