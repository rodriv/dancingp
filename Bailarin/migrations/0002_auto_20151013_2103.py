# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Bailarin', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Curriculum',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('formacion_academica', models.TextField()),
                ('experiencia_profesional', models.TextField()),
                ('idiomas', models.TextField()),
                ('video1', models.URLField()),
                ('video2', models.URLField()),
                ('video3', models.URLField()),
            ],
        ),
        migrations.RenameModel(
            old_name='Provincias',
            new_name='Provincia',
        ),
        migrations.AddField(
            model_name='bailarin',
            name='foto_perfil',
            field=models.ImageField(blank=True, null=True, upload_to='perfiles'),
        ),
        migrations.AddField(
            model_name='curriculum',
            name='bailarin',
            field=models.OneToOneField(to='Bailarin.Bailarin'),
        ),
    ]
