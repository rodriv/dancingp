# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('Bailarin', '0003_remove_bailarin_nick'),
    ]

    operations = [
        migrations.AddField(
            model_name='bailarin',
            name='telefono',
            field=models.CharField(validators=[django.core.validators.RegexValidator(message="El teléfono debe tener el formato: '+999999999'. Hasta 15 dígitos.", regex='^\\+?1?\\d{9,15}$')], max_length=15, blank=True),
        ),
    ]
