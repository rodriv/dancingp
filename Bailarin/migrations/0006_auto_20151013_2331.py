# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Bailarin', '0005_auto_20151013_2249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bailarin',
            name='telefono',
            field=models.CharField(max_length=15, blank=True),
        ),
        migrations.AlterField(
            model_name='curriculum',
            name='video1',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='curriculum',
            name='video2',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='curriculum',
            name='video3',
            field=models.URLField(null=True),
        ),
    ]
