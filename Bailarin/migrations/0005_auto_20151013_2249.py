# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('Bailarin', '0004_bailarin_telefono'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bailarin',
            name='telefono',
            field=models.CharField(blank=True, max_length=15, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b"El telefono debe tener el formato: '+999999999'. Hasta 15 digitos.")]),
        ),
    ]
