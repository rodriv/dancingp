from django.forms import ModelForm
from Bailarin.models import Curriculum

class CVModelForm(ModelForm):
	class Meta:
		model = Curriculum
		fields = ['formacion_academica','experiencia_profesional','idiomas','video1','video2','video3']