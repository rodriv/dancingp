from django.shortcuts import render, render_to_response
from django.template import RequestContext
from Bailarin.models import Bailarin

def index(request):
	bailarines = Bailarin.objects.all().order_by("-fecha_alta")[:3]
	if request.user.is_authenticated():
		return render_to_response('index.html', {"Bailarines" : bailarines, "U": request.user}, context_instance=RequestContext(request))
	else:
		return render_to_response('index.html', {"Bailarines" : bailarines}, context_instance=RequestContext(request))


def mision(request):
	return render_to_response('mision.html')


def contacto(request):
	return render_to_response('contacto.html')

def desarrollador(request):
	return render_to_response('desarrollador.html')